//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import{RouterModule, Routes} from"@angular/router";

//Components
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { LivreListComponent } from './livre-list/livre-list.component';
import { SingleLivreComponent } from './livre-list/single-livre/single-livre.component';
import { LivreFormComponent } from './livre-list/livre-form/livre-form.component';
import { HeaderComponent } from './header/header.component';
import { TitreComponent } from './titre/titre.component';
import { AchatComponent } from './achat/achat.component';



//services
import { AuthService } from "./services/auth.service";
import { AuthGuardService } from "./services/auth-guard.service";
import { LivresService } from "./services/livres.service";

const appRoutes : Routes =[
  {path:'auth/signup', component: SignupComponent},
  {path:'auth/signin', component: SigninComponent},
  {path:'livres', canActivate:[AuthGuardService],component: LivreListComponent},
  {path:'livres/new',canActivate:[AuthGuardService],component: LivreFormComponent},
  {path:'livres/view/:id', component:SingleLivreComponent},
  {path:'', redirectTo:'livres', pathMatch:'full'},
  {path:'**', redirectTo:'livres'}
]

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    LivreListComponent,
    SingleLivreComponent,
    LivreFormComponent,
    HeaderComponent,
    TitreComponent,
    AchatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    LivresService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
