export class Livre{
  photo:string;
  synopsis:string;

  constructor(public title: string, public author: string){

  }
}
