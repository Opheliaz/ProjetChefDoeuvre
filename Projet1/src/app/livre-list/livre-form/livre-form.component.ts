import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {LivresService} from "../../services/livres.service";
import {Router} from "@angular/router";
import {Livre} from "../../models/Livre.model";

@Component({
  selector: 'app-livre-form',
  templateUrl: './livre-form.component.html',
  styleUrls: ['./livre-form.component.scss']
})
export class LivreFormComponent implements OnInit {

  livreForm: FormGroup;
  fileIsUploading = false;
  fileUrl:string;
  fileUploaded = false;

  constructor( private formBuilder: FormBuilder,
               private livresService: LivresService,
               private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm(){
    this.livreForm  = this.formBuilder.group({
      title:['', Validators.required],
      author:['', Validators.required]

    });

  }
  onSaveLivre(){
    const title = this.livreForm.get('title').value;
    const author = this.livreForm.get('author').value;
    const newLivre = new Livre(title, author);
    if(this.fileUrl && this.fileUrl !==''){
      newLivre.photo = this.fileUrl;
    }
    this.livresService.createNewLivres(newLivre);
    this.router.navigate(['/livres']);
  }

  onUploadFile(file:File){
    this.fileIsUploading = true;
    this.livresService.uploadFile(file).then(
      (url: string)=>{
        this.fileUrl= url;
        this.fileIsUploading = false;
        this.fileUploaded = true;

    })
  }
  detectFiles(event){
  this.onUploadFile(event.target.files[0]);
  }
}
