import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleLivreComponent } from './single-livre.component';

describe('SingleLivreComponent', () => {
  let component: SingleLivreComponent;
  let fixture: ComponentFixture<SingleLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
