import { Component, OnInit } from '@angular/core';
import{ Livre } from '../../models/Livre.model';
import {ActivatedRoute, Router}from "@angular/router";
import {LivresService} from "../../services/livres.service";
@Component({
  selector: 'app-single-livre',
  templateUrl: './single-livre.component.html',
  styleUrls: ['./single-livre.component.scss']
})
export class SingleLivreComponent implements OnInit {

  livre: Livre;
  constructor(private route: ActivatedRoute,
              private livresService: LivresService,
              private router: Router){}
  ngOnInit(){
    this.livre = new Livre('','');
    const id = this.route.snapshot.params['id'];
    this.livresService.getSingleLivre(+id).then(
      (livre: Livre) =>{
      this.livre = livre;
      }
    );
  }
  onBack(){
    this.router.navigate(['/livres']);
  }
}
