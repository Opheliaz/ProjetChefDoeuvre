import {Component, OnDestroy, OnInit} from '@angular/core';
import {Livre} from'../models/Livre.model';
import { Subscription} from'rxjs';
import {LivresService} from "../services/livres.service";
import{Router} from'@angular/router';

@Component({
  selector: 'app-livre-list',
  templateUrl: './livre-list.component.html',
  styleUrls: ['./livre-list.component.scss']
})
export class LivreListComponent implements OnInit, OnDestroy{
  livres : Livre[];
  livresSubscription:Subscription;

  constructor(private livresService: LivresService, private router: Router ) { }
i
  ngOnInit() {
    this.livresSubscription = this.livresService.livresSubject.subscribe(
      (livres: Livre[])=>{
        this.livres = livres;
      }
    );
    this.livresService.getLivres();
    this.livresService.emitLivres();
  }

  onNewLivre(){
    this.router.navigate(['/livres','new']);
  }

  onDeleteLivres(livre: Livre) {
    this.livresService.removeLivre(livre);
  }

  onViewLivre(id:number){

      this.router.navigate(['/livre', 'view', id]);
  }

  ngOnDestroy(){
  this.livresSubscription.unsubscribe();
  }

}




