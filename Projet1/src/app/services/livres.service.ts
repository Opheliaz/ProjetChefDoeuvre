import { Injectable } from '@angular/core';
import {Livre } from '../models/Livre.model';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LivresService {

  livres: Livre[] = [];
  livresSubject = new Subject<Livre[]>();
  constructor() { }

  emitLivres(){
    this.livresSubject.next(this.livres);

  }
  saveLivres(){
    firebase.database().ref('/livres').set(this.livres);
  }

  getLivres(){
    firebase.database().ref('/livres').on('value', (data)=> {
      this.livres = data.val()? data.val(): [];
      this.emitLivres();
    });
  }

  getSingleLivre(id: number){
      return new Promise ((resolve, reject)=>{
        firebase.database().ref('/livres/' +id).once('value').then(
          (data)=>{
            resolve(data.val());
          }, (error)=>{
            reject(error);
            }
          );
        }
      );
    }

    createNewLivres(newLivre: Livre){
    this.livres.push(newLivre);
    this.saveLivres();
    this.emitLivres();
    }

    removeLivre(livre:Livre){
    if(livre.photo){
      const storageRef = firebase.storage().refFromURL(livre.photo);
      storageRef.delete().then(
        ()=>{
          console.log('Photo supprimée  ! ')
        }
      ).catch((error)=>{
        console.log('fichier non trouvé :' + error);
      })
    }
    const livreIndexToRemove = this.livres.findIndex(
      (livreEl)=>{
        if(livreEl === livre){
          return true;
        }
      }
    );
    this.livres.splice(livreIndexToRemove, 1);
    this.saveLivres();
    this.emitLivres();
    }
    uploadFile(file:File){
     return new Promise(
       (resolve, reject)=>{
       const almostUniqueFileName = Date.now().toString();
      const upload = firebase.storage().ref()
        .child('image/'+ almostUniqueFileName + file.name)
        .put(file);
      upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
        ()=>{
        console.log('Chargement...');

        }
        ,
        (error)=>{
        console.log('Erreur de chargement: '+ error);
        reject();
        },
        ()=>{
        resolve(upload.snapshot.downloadURL);
        }
      );
     }
     );
    }
}
