import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(){
    var config = {
      apiKey: "AIzaSyCGF4hyqglZZPppgINlDGhDD4mcnW2IKh8",
      authDomain: "livres-808d9.firebaseapp.com",
      databaseURL: "https://livres-808d9.firebaseio.com",
      projectId: "livres-808d9",
      storageBucket: "",
      messagingSenderId: "757485250541"
    };
    firebase.initializeApp(config)
  }
}
